package lv.sda.demo;

import lv.sda.demo.model.User;
import lv.sda.demo.service.UserInputService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	private UserInputService userService;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		User user = userService.createUserFromInput();
		System.out.println(user.getName() + " " + user.getSurname());
	}

	/*
	1. Add Book class with fields: Author, issueDate, isbn;
	2. Add service class to fetch book from remote localtion (use empty/dummy methods)
	3. In main method show usage of created service

	4* Make UserInputService.askUser method work asking user's name and surname from application console
	     (hints, java.util.scanner, google, @Autowired)
	 */


}
