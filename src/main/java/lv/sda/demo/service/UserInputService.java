package lv.sda.demo.service;

import lv.sda.demo.model.User;
import org.springframework.stereotype.Service;

import java.util.Scanner;

//this service processes user input from console
@Service
public class UserInputService {

    public User createUserFromInput() {
        User user = new User();
        user.setName(askUser("Please, enter your name?"));
        user.setSurname(askUser("Please enter your surname?"));
        return user;
    }

    private String askUser(String question) {
        Scanner sc = new Scanner(System.in);
        askUser(question);
        return sc.nextLine();
    }

}
